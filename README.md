# An LT5537 Based Power Detector

## Features

- Low Frequency to over 600 MHz Operation
- 83dB Dynamic Range with $\pm 1$ dB Nonlinearity at 200 MHz
- Sensitivity –76 dBm or Better at 200 MHz
- On board EEPROM for calibration parameters.
- Optional access to detector output voltage.

## Documentation

Full documentation for the detector is available at
[RF Blocks](https://rfblocks.org/boards/LTC5582-Power-Detector.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
